Source: r-bioc-metapod
Section: gnu-r
Priority: optional
Maintainer: Debian R Packages Maintainers <r-pkg-team@alioth-lists.debian.net>
Uploaders: Andreas Tille <tille@debian.org>
Vcs-Browser: https://salsa.debian.org/r-pkg-team/r-bioc-metapod
Vcs-Git: https://salsa.debian.org/r-pkg-team/r-bioc-metapod.git
Homepage: https://bioconductor.org/packages/metapod/
Standards-Version: 4.7.0
Rules-Requires-Root: no
Build-Depends: debhelper-compat (= 13),
               dh-r,
               r-base-dev,
               r-cran-rcpp,
               architecture-is-64-bit
Testsuite: autopkgtest-pkg-r

Package: r-bioc-metapod
Architecture: any
Depends: ${R:Depends},
         ${shlibs:Depends},
         ${misc:Depends}
Recommends: ${R:Recommends}
Suggests: ${R:Suggests}
Description: GNU R meta-analyses on P-Values of differential analyses
 Implements a variety of methods for combining p-values in differential
 analyses of genome-scale datasets. Functions can combine p-values across
 different tests in the same analysis (e.g., genomic windows in ChIP-seq,
 exons in RNA-seq) or for corresponding tests across separate analyses
 (e.g., replicated comparisons, effect of different treatment conditions).
 Support is provided for handling log-transformed input p-values, missing
 values and weighting where appropriate.
